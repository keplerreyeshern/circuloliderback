<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Brand;
use App\Models\capacityModel;
use App\Models\colorModel;
use App\Models\Document;
use App\Models\Manager;
use App\Models\Plan;
use App\Models\Promotion;
use App\Models\Quotation;
use App\Models\Region;
use App\Models\Store;
use App\Models\TeamCapacity;
use App\Models\TeamColor;
use App\Models\TeamModel;
use App\Models\Vendor;
use App\Models\Client;
use Illuminate\Http\Request;

class DataController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function dataSeller()
    {
        $user = auth()->user();
        $seller = Vendor::where('user_id', $user->id)->first();
        $brands = Brand::get();
        $team_models = TeamModel::get();
        $colors = TeamColor::get();
        $capacities = TeamCapacity::get();
        $plans = Plan::get();
        $colors_teams = colorModel::get();
        $capacities_teams = capacityModel::get();
        $promotions = Promotion::where('active', true)->get();
        $quotations = Quotation::where('vendor_id', $seller->id)->get();
        $documents = Document::where('vendor_id', $seller->id)->get();
        $clients = Client::where('vendor_id', $seller->id)->get();
        $stores = Store::get();
        $managers = Manager::get();
        $regions = Region::get();
        $result = [
            'seller' => $seller,
            'brands' => $brands,
            'team_models' => $team_models,
            'colors' => $colors,
            'capacities' => $capacities,
            'plans' => $plans,
            'colors_teams' => $colors_teams,
            'capacities_teams' => $capacities_teams,
            'promotions' => $promotions,
            'quotations' => $quotations,
            'documents' => $documents,
            'clients' => $clients,
            'stores' => $stores,
            'managers' => $managers,
            'regions' => $regions
        ];
        return json_encode($result);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function dataUser()
    {
        $user = auth()->user();

        $result = [
        ];
        return json_encode($result);
    }
}
