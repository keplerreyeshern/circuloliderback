<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Prospector;
use App\Models\User;
use App\Models\Vendor;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str as Str;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::all();
        $sellers = Vendor::all();
        $result = [
            'users' => $users,
            'sellers' => $sellers
        ];
        return json_encode($result);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $length = 5;
        $prospectorsLength = 1;
        $prospectors = Prospector::all();
        foreach ($prospectors as $prospector){
            $prospectorsLength = $prospectorsLength + 1;
        }
        $user = New User();
        $user->name = $request['name'];
        $user->email = $request['email'];
        $user->profile = $request['profile'];
        $user->active = true;
        $user->password = Hash::make($request['password']);
        $user->save();

        if ($request['profile'] == 'user'){
            $seller = Vendor::where('code', $request['code'])->first();
            $length = 10;
            $prospector = new Prospector();
            $prospector->name = $request['name'];
            $prospector->slug = Str::slug($request['name']);
            $prospector->code = substr(str_repeat(0, $length).$prospectorsLength, - $length);
            $prospector->user_id = $user->id;
            $prospector->vendor_id = $seller->id;
            $prospector->save();
        }

        return json_encode($user);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::findOrFail($id);
        return $user;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::findOrFail($id);
        if($user->active){
            $user->active = false;
        } else {
            $user->active = true;
        }
        $user->save();

        return $user;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = User::findOrFail($id);
        $user->name = $request['name'];
        $user->email = $request['email'];
        if($request['checkbox'] == 1) {
            $user->password = Hash::make($request['password']);
        }
        $user->save();

        return $user;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::findOrFail($id);
        $user->delete();

        return $user;
    }
}
