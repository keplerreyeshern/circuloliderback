<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class capacityModel extends Model
{
    use HasFactory;

    protected $table = 'table_team_capacity_team_model';
}
