<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class colorModel extends Model
{
    use HasFactory;

    protected $table = 'table_team_color_team_model';
}
