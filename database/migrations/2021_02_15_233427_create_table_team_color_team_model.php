<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableTeamColorTeamModel extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('table_team_color_team_model', function (Blueprint $table) {
            $table->id();
            $table->foreignId('team_color_id')
                ->references('id')
                ->on('team_colors')
                ->onDelete('cascade');
            $table->foreignId('team_model_id')
                ->references('id')
                ->on('team_models')
                ->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('table_team_color_team_model');
    }
}
