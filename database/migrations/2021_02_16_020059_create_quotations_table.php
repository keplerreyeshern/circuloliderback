<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateQuotationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('quotations', function (Blueprint $table) {
            $table->id();
            $table->foreignId('vendor_id')
                ->references('id')
                ->on('vendors')
                ->onDelete('cascade');
            $table->foreignId('team_model_id')
                ->nullable('cascade')
                ->references('id')
                ->on('team_models')
                ->onDelete('cascade');
            $table->foreignId('plan_id')
                ->references('id')
                ->on('plans')
                ->onDelete('cascade');
            $table->foreignId('client_id')
                ->references('id')
                ->on('clients')
                ->onDelete('cascade');
            $table->boolean('team');
            $table->boolean('activation')->default(true);
            $table->boolean('protect_secure')->default(false);
            $table->boolean('add_control')->default(false);
            $table->boolean('payment')->default(false);
            $table->integer('term')->default(36);
            $table->boolean('active')->default(true);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('quotations');
    }
}
